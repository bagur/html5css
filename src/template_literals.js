const horse = {
    name: 'Topher The_horse',
    size: 'large',
    skils: ['jousting', 'racing'],
    age: 7
}

// bad code
let bio = horse.name + ' is a ' + horse.size + ' horse skilled in ' + horse.skils.join(' & ');
console.log(bio);

// good code
const {name, size, skils} = horse;

bio = `${name} is a ${size} horse skilled in ${skils.join(' & ')}` 
console.log(bio);

// advanced tag example
function horseAge(str, age){
    const ageStr = age > 5 ? 'old' : 'young';
    return `${str[0]}${ageStr} at ${age} years`;
}

const bio2 = horseAge`This horse is ${horse.age}`;

console.log(bio2);

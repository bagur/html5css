
const foo = {name: "harry potter", age: 31, nervous: false};
const bar = {name: "ron weasley", age: 30, nervous: false};
const baz = {name: "hermione granger", age: 29, nervous: true};

// bad practice
console.log('%c bad practice', 'color: red; font-weight: bold') // extra
console.log(foo);
console.log(bar);
console.log(baz);

// best practice
console.log('%c best practice', 'color: orange; font-weight: bold') // extra
console.log({foo, bar, baz}); // computed property names
console.table([foo, bar, baz]); // as a table

// Console.time
console.time('looper')

let i = 0;
while (i < 1000000) {i ++}

console.timeEnd('looper')

// stack trace logs

const deleteMe = () =>console.trace('bye bye database')

deleteMe()
deleteMe()
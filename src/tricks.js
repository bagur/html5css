const turtle = {
    name: 'Bob the turtle',
    legs: 4,
    shell: true,
    type: 'amphibious',
    meal: 10,
    diet: 'berries'
}

// bad code
function feed(animal){
    return `Feed ${animal.name} eat ${animal.meal} kilos of ${animal.diet}`;
}

console.log(feed(turtle));

// good code 
function feed({name, meal, diet}){
    return `Feed ${name} ${meal} kilos of ${diet}`;
}

console.log(feed(turtle));

// or
function feed(animal){
    const {name, meal, diet} = animal;
    return `Feed ${name} ${meal} kilos of ${diet}`;
}
console.log(feed(turtle));


